"use strict";

const { ServiceBroker } = require("moleculer");
const { ValidationError } = require("moleculer").Errors;
const TestService = require("../../services/shop.service");

describe("Test 'shop' service", () => {
	let broker = new ServiceBroker();
	broker.createService(TestService);

	beforeAll(() => broker.start());
	afterAll(() => broker.stop());
  //TODO : unit test all
	describe("Test 'shop.shopIDs' action", () => {

		it("should reject an ValidationError", () => {
			expect(broker.call("shop.shopIDs")).rejects.toBeInstanceOf(ValidationError);
		});

	});

});

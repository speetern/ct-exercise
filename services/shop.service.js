"use strict";

const request = require('request');
const util = require('util');
const APIkey = '123456789'; //NOTE: update APIkey with your own
const fs = require('fs');
const os = require("os");

module.exports = {
	name: "shop",

		/**
		 * Service settings
		 */
		settings: {

		},

		/**
		 * Service dependencies
		 */
		dependencies: [],

		/**
		 * Actions
		 */
		actions: {
			/**
			 * Process Shop IDs
			 *
			 * @param {String} IDs - comma separated list of shop IDs
			 */
			shopIDs: {
				params: {
					IDs: "string"
				},
				handler(ctx) {
					var shopIDsArray = ctx.params.IDs.split(',');
					var numberOfShopIds = shopIDsArray.length;

					/** Function : saveFile
					**  Purpose : write a shops listing to file system async
					**/
					function saveFile(shopFilePath,body) {
						fs.writeFile(shopFilePath, body, function(err) {
								if(err) {
									return console.log(err);
								}
								console.log("The file was successfully saved to: "+ shopFilePath);
						});
			  	};

					/** Function : checkAddedListings
					**  Purpose : check to see if any listings were added and add to listings if so
					**/
					function checkAddedListings(mtime,originalCreation,state,listingID,listingTitle,addedListings) {
						if( (mtime < originalCreation) && (state == "active") ){//check listing original_creation_tsz
							console.log('added Listing: ', listingID);
							var listingAdded = listingID + " " + listingTitle;
							addedListings.push(listingAdded);
						}
					};

					/** Function : checkRemovedListings
					**  Purpose : check to see if any listings were removed and add to removed listings if so
					**/
					function checkRemovedListings(mtime,originalCreation,state,listingID,listingTitle,removedListings,lastModified) {
						if( (mtime < lastModified) && (state == "removed") ){ //last_modified_tsz	state	public	none	string	One of active, removed, sold_out, expired, alchemy, edit, create, private, or unavailable.
							console.log('removed Listing: ', listingID);
							var listingRemoved = listingID + " " + listingTitle;
							removedListings.push(listingRemoved);
						}
					};

          /** Function : processShop
					**  Purpose : check if file exists in order to output added and removed listings
					**/
					function processShop(options,shopFilePath,body) {
						console.log('shopID:', options.shopID);
						var shopObject = JSON.parse(body);
						var responseStr = " Shop ID " + options.shopID+ ": ";
						var noChangeStr = "No Changes since last sync";
						var addedListings = [];
						var removedListings = [];
						if (fs.existsSync(shopFilePath) && shopObject.results!=null) { //check if file already exists
								var stats = fs.statSync(shopFilePath);
								var numberOfListings = shopObject.results.length;
								var mtime = new Date(util.inspect(stats.mtime));
								mtime = mtime.getTime();
								console.log("last modified: "+ mtime);
								// loop over listings per shop
								for(var i = 0; i<numberOfListings; i++){
									var listingID = shopObject.results[i].listing_id;
									var listingTitle = shopObject.results[i].title;
									var state = shopObject.results[i].state;
									var listingID = shopObject.results[i].listing_id;
									var originalCreation = shopObject.results[i].original_creation_tsz;
									var lastModified= shopObject.results[i].last_modified_tsz;
									// check for added Listings
									checkAddedListings(mtime,originalCreation,state,listingID,listingTitle,addedListings);
									// check for removed Listings
									checkRemovedListings(mtime,originalCreation,state,listingID,listingTitle,removedListings,lastModified);
								}
					  }else{
								responseStr = responseStr + noChangeStr;
					  }
				  	var output = responseStr + " + Added Listings: " + addedListings + " - Removed Listings: " + removedListings;
				  	return output;
			  	};

					// main loop over shopIDsArray and process each shop
					const promises = [];
					for(var j =0;j < numberOfShopIds;j++) {
							var promise = new Promise(function(resolve, reject) {
									var url = 'https://openapi.etsy.com/v2/shops/' + shopIDsArray[j]+ '/listings/active?api_key=' + APIkey;
									var options = {
							        url: url,
											shopID: shopIDsArray[j],
							        headers: {
							            'User-Agent': 'request'
							        }
							    };
                  //request shop listings by shop ids
					        request.get(options, function(err, resp, body) {
					            if (err) {
											  	console.log('error:', err);
					                reject(err);
					            }else if(resp.statusCode !== 200 ){
													console.log('statusCode:', resp && resp.statusCode);
													reject(err);
											} else {
												//process shop
												var shopFilePath = "./ShopListings/" + options.shopID + ".txt";;
												var outputStr = processShop(options,shopFilePath,body);
												// write to file
												saveFile(shopFilePath,body);
												resolve(outputStr);
					            }
					        })
					    });
							promises.push(promise);
					}
          //wait until all shop ids are processed then return values for output
				  return Promise.all(promises).then(function(values) {
  				   	console.log("output : "+values);
					  	return Promise.resolve(values);
				  });

				}
			}
		},

		/**
		 * Events
		 */
		events: {

		},

		/**
		 * Methods
		 */
		methods: {

		},

		/**
		 * Service created lifecycle event handler
		 */
		created() {

		},

		/**
		 * Service started lifecycle event handler
		 */
		started() {

		},

		/**
		 * Service stopped lifecycle event handler
		 */
		stopped() {

		}
	};

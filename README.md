[![Moleculer](https://img.shields.io/badge/Powered%20by-Moleculer-green.svg?colorB=0e83cd)](https://moleculer.services)

# moleculer-demo for ct-exercise

## Build Setup

``` bash
# Install dependencies
npm install

# Start developing with REPL
npm run dev

# Start production
npm start

# Run unit tests
npm test

# Run continuous test mode
npm run ci

# Run ESLint
npm run lint
```

## Run in Docker

```bash
$ docker-compose up -d --build
```
NOTES:
- This is a simple service to synchronize Etsy shop listings to several files (one per shop ID), outputting
what has been added or removed since the last run.  The ShopListings folder contains the latest data per Shop ID.
- Instructions:
 Set the APIKey to your own in shop.service.js:

    const APIkey = 'your_APIkey'; 

 After running 'npm run dev', simply curl to the appropriate endpoint with a comma separated list of shop IDs as parameters.
"curl -X GET http://0.0.0.0:3000/api/shop/shopIDs?IDs=19113675,1544484116,1544486547"



Sample Output to Command line from Client curls:

Nicholass-MacBook-Air:~ nicholasspeeter$ curl -X GET http://0.0.0.0:3000/api/shop/shopIDs?IDs=19113421,19113675
[" Shop ID 19113421:  + Added Listings:  - Removed Listings: "," Shop ID 19113675: No Changes since last sync + Added Listings:  - Removed Listings: "]
Nicholass-MacBook-Air:~ nicholasspeeter$ 
Nicholass-MacBook-Air:~ nicholasspeeter$ curl -X GET http://0.0.0.0:3000/api/shop/shopIDs?IDs=19113421,19113675
[" Shop ID 19113421:  + Added Listings: 666398855 Multi occasion spa baskets,652472174 Floral Bridesmaids robes- or gift for that special female in your life- customization upon request,666303253 Personalized sweatshirts- most commonly used for bridal parties,666292871 The weekend getaway basket - Removed Listings: "," Shop ID 19113675:  + Added Listings: 666516793 Faux Leather Bow Leather Bows Surprise Doll Hairbow,652532130 Leather Hairbow Clip, Faux Leather Bow, Christmas Character Leather Bow,666366691 Leather Hairbow Clip, Faux Leather Bows, Leather Bows, Silver Glitter Leather Bow,666306653 Leather Hairbow Clip, Leather Bows, Faux Leather Bows, Fancy Cartoon Girl Leather Bow - Removed Listings: "]
Nicholass-MacBook-Air:~ nicholasspeeter$ 
Nicholass-MacBook-Air:~ nicholasspeeter$ curl -X GET http://0.0.0.0:3000/api/shop/shopIDs?IDs=19113421,19113675
[" Shop ID 19113421:  + Added Listings:  - Removed Listings: "," Shop ID 19113675:  + Added Listings:  - Removed Listings: "]
Nicholass-MacBook-Air:~ nicholasspeeter$ 
Nicholass-MacBook-Air:~ nicholasspeeter$ 
Nicholass-MacBook-Air:~ nicholasspeeter$ curl -X GET http://0.0.0.0:3000/api/shop/shopIDs?IDs=19113421,19113675
[" Shop ID 19113421:  + Added Listings:  - Removed Listings: 666398855,652472174,666303253,666292871"," Shop ID 19113675:  + Added Listings:  - Removed Listings: 666516793,652532130,666366691,666306653"]
Nicholass-MacBook-Air:~ nicholasspeeter$ curl -X GET http://0.0.0.0:3000/api/shop/shopIDs?IDs=19113421,19113675
[" Shop ID 19113421:  + Added Listings:  - Removed Listings: 666398855 Multi occasion spa baskets,652472174 Floral Bridesmaids robes- or gift for that special female in your life- customization upon request,666303253 Personalized sweatshirts- most commonly used for bridal parties,666292871 The weekend getaway basket"," Shop ID 19113675:  + Added Listings:  - Removed Listings: 666516793 Faux Leather Bow Leather Bows Surprise Doll Hairbow,652532130 Leather Hairbow Clip, Faux Leather Bow, Christmas Character Leather Bow,666366691 Leather Hairbow Clip, Faux Leather Bows, Leather Bows, Silver Glitter Leather Bow,666306653 Leather Hairbow Clip, Leather Bows, Faux Leather Bows, Fancy Cartoon Girl Leather Bow"]
Nicholass-MacBook-Air:~ nicholasspeeter$ 
Nicholass-MacBook-Air:~ nicholasspeeter$ curl -X GET http://0.0.0.0:3000/api/shop/shopIDs?IDs=19113421,19113675
[" Shop ID 19113421:  + Added Listings:  - Removed Listings: "," Shop ID 19113675:  + Added Listings:  - Removed Listings: "]
Nicholass-MacBook-Air:~ nicholasspeeter$ 
Nicholass-MacBook-Air:~ nicholasspeeter$ curl -X GET http://0.0.0.0:3000/api/shop/shopIDs?IDs=19113421,19113675
[" Shop ID 19113421:  + Added Listings:  - Removed Listings: "," Shop ID 19113675:  + Added Listings:  - Removed Listings: "]


Sample Server side output from 'npm run dev' command line:

[2018-12-11T23:23:09.595Z] INFO  nicholass-macbook-air.local-2298/REGISTRY: 'shop' service is registered.
[2018-12-11T23:23:10.764Z] INFO  nicholass-macbook-air.local-2298/API: => GET /api/shop/shopIDs?IDs=19113421,19113675
[2018-12-11T23:23:10.765Z] INFO  nicholass-macbook-air.local-2298/API:    Call 'shop.shopIDs' action
shopID: 19113675
last modified: 1544570159029
The file was successfully saved to: ./ShopListings/19113675.txt
shopID: 19113421
last modified: 1544570159095
output :  Shop ID 19113421:  + Added Listings:  - Removed Listings: , Shop ID 19113675:  + Added Listings:  - Removed Listings: 
[2018-12-11T23:23:11.171Z] INFO  nicholass-macbook-air.local-2298/API: <= 200 GET /shop/shopIDs?IDs=19113421,19113675 [+406.393 ms]
[2018-12-11T23:23:11.171Z] INFO  nicholass-macbook-air.local-2298/API: 
The file was successfully saved to: ./ShopListings/19113421.txt
[2018-12-11T23:23:17.901Z] INFO  nicholass-macbook-air.local-2298/API: => GET /api/shop/shopIDs?IDs=19113421
[2018-12-11T23:23:17.902Z] INFO  nicholass-macbook-air.local-2298/API:    Call 'shop.shopIDs' action
shopID: 19113421
last modified: 1544570591174
output :  Shop ID 19113421:  + Added Listings:  - Removed Listings: 
[2018-12-11T23:23:18.263Z] INFO  nicholass-macbook-air.local-2298/API: <= 200 GET /shop/shopIDs?IDs=19113421 [+361.845 ms]
[2018-12-11T23:23:18.263Z] INFO  nicholass-macbook-air.local-2298/API: 
The file was successfully saved to: ./ShopListings/19113421.txt
[2018-12-11T23:23:38.274Z] INFO  nicholass-macbook-air.local-2298/API: => GET /api/shop/shopIDs?IDs=19113421,19113675
[2018-12-11T23:23:38.275Z] INFO  nicholass-macbook-air.local-2298/API:    Call 'shop.shopIDs' action
shopID: 19113675
last modified: 1544570591161
shopID: 19113421
last modified: 1544570598265
output :  Shop ID 19113421:  + Added Listings:  - Removed Listings: , Shop ID 19113675:  + Added Listings:  - Removed Listings: 
[2018-12-11T23:23:38.628Z] INFO  nicholass-macbook-air.local-2298/API: <= 200 GET /shop/shopIDs?IDs=19113421,19113675 [+353.705 ms]
[2018-12-11T23:23:38.628Z] INFO  nicholass-macbook-air.local-2298/API: 
The file was successfully saved to: ./ShopListings/19113675.txt
The file was successfully saved to: ./ShopListings/19113421.txt





